// API caller methods for accounts.

import axios from 'axios';

import { V1_API_URL_PRODUCTION_BASE } from '../config/base.js';

export const getAccountByAccountIdPromise = (accountId) => {
    return axios.get(V1_API_URL_PRODUCTION_BASE + 'account/' + accountId)
}
