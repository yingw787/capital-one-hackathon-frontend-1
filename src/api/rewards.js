// API caller methods for customers.

import axios from 'axios';

import { V1_API_URL_PRODUCTION_BASE } from '../config/base.js';

export const getRewardsByAccountIdPromise = (accountId) => {
    return axios.get(V1_API_URL_PRODUCTION_BASE + 'rewards/' + accountId)
}
