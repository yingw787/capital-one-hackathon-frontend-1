// API caller methods for customers.

import axios from 'axios';

import { V1_API_URL_PRODUCTION_BASE } from '../config/base.js';

export const getTotalRewardsRemainingByAccountIdPromise = (accountId) => {
    return axios.get(V1_API_URL_PRODUCTION_BASE + 'total_rewards_remaining/' + accountId)
}
