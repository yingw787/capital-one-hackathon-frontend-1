import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import './HomeCard.css';
import logo from '../../../static/logos/capital_one.svg';

class HomeCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            accountId: '100100000',
            customerId: '100110000',
            fireRedirect: false
        };

        this.updateAccountId = this.updateAccountId.bind(this);
        this.updateCustomerId = this.updateCustomerId.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    updateAccountId(event) {
        this.setState({
            accountId: event.target.value
        });
    }

    updateCustomerId(event) {
        this.setState({
            customerId: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            fireRedirect: true
        });
    }

    render() {
        const { from } = this.props.location.state || '/'
        const { fireRedirect, accountId, customerId } = this.state

        return (
            <div className="HomeCard-container">
                <img src={logo} className="HomeCard-logo" alt="logo" />
                <form className="HomeCard-entry-form" onSubmit={this.handleSubmit}>
                    <div className="HomeCard-input-title">
                        Account ID:
                    </div>
                    <input
                        type="text"
                        className="HomeCard-input"
                        value={this.state.accountId}
                        onChange={this.updateAccountId}
                    />
                    <br />
                    <div className="HomeCard-input-title">
                        Customer ID:
                    </div>
                    <input
                        type="text"
                        className="HomeCard-input"
                        value={this.state.customerId}
                        onChange={this.updateCustomerId}
                    />
                    <div className="HomeCard-input-submit-wrapper">
                        <input
                            type="submit"
                            value="Make my life better!"
                            className="HomeCard-input-submit"
                        />
                    </div>
                    {
                        fireRedirect &&
                        <Redirect to={ from || '/dashboard?accountId=' + accountId + '&customerId=' + customerId } />
                    }
                </form>
            </div>
        );
    }
}

export default HomeCard;
