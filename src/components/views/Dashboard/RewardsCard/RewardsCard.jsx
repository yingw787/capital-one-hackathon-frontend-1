import React, { Component } from 'react';

import './RewardsCard.css';

class RewardsCard extends Component {
    render() {
        const { rewardsInformation, totalRewardsRemainingInformation } = this.props;

        return (
            <div className="RewardsCard-container">
                <div className="RewardsCard-title">
                    Rewards Information
                </div>

                <div className="RewardsCard-entry"><b>Metadata:</b></div>
                <div className="RewardsCard-entry">You have {rewardsInformation.length} rewards in your reward history.</div>

                <br />
                <div className="RewardsCard-entry"><b>Metadata:</b></div>
                <div className="RewardsCard-entry">You have {totalRewardsRemainingInformation.length} rewards remaining.</div>

                <br />
                <br />
            </div>
        );
    }
}

export default RewardsCard;
