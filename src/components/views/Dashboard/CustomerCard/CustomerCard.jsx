import React, { Component } from 'react';

import './CustomerCard.css';

class CustomerCard extends Component {
    render() {
        const { customerInformation } = this.props;

        return (
            <div className="CustomerCard-container">
                <div className="CustomerCard-title">
                    Personal Information
                </div>
                <div className="CustomerCard-entry"><b>Personal Information:</b></div>
                <div className="CustomerCard-entry">First Name: {customerInformation.first_name}</div>
                <div className="CustomerCard-entry">Last Name: {customerInformation.last_name}</div>
                <div className="CustomerCard-entry">Gender: {customerInformation.gender}</div>
                <div className="CustomerCard-entry">Phone Number: {customerInformation.phone_number}</div>
                <div className="CustomerCard-entry">Email Address: {customerInformation.email}</div>

                <br />
                <div className="CustomerCard-entry"><b>Primary Address Information:</b></div>
                <div className="CustomerCard-entry">Street: {customerInformation.address}</div>
                <div className="CustomerCard-entry">City: {customerInformation.city}</div>
                <div className="CustomerCard-entry">State: {customerInformation.state}</div>
                <div className="CustomerCard-entry">Zipcode: {customerInformation.zipcode}</div>
                <div className="CustomerCard-entry">Country: {customerInformation.country}</div>

                <br />
                <br />
            </div>
        );
    }
}

export default CustomerCard;
