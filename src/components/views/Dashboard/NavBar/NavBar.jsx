import React, { Component } from 'react';

import './NavBar.css';
import logo from '../../../../static/logos/capital_one.svg';

class NavBar extends Component {
    render() {
        const {customerFirstName} = this.props;

        return (
            <div className="NavBar-container">
                <img src={logo} alt="logo" className="NavBar-logo" />
                <div className="NavBar-nav">
                    Hello, {customerFirstName}
                </div>
            </div>
        );
    }
}

export default NavBar;
