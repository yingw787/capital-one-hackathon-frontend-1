import React, { Component } from 'react';

import './PaymentsCard.css';

class PaymentsCard extends Component {
    render() {
        const { paymentsInformation } = this.props;

        return (
            <div className="PaymentsCard-container">
                <div className="PaymentsCard-title">
                    Payments Information
                </div>

                <div className="PaymentsCard-entry"><b>Metadata:</b></div>
                <div className="PaymentsCard-entry">You have {paymentsInformation.length} payments in your payment history.</div>

                <br />
                <br />
            </div>
        );
    }
}

export default PaymentsCard;
