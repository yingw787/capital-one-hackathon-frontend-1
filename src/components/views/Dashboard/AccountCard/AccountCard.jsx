import React, { Component } from 'react';

import './AccountCard.css';

class AccountCard extends Component {
    render() {
        const { accountInformation, customerInformation } = this.props;

        return (
            <div className="AccountCard-container">
                <div className="AccountCard-title">
                    Account Information
                </div>
                <div className="AccountCard-entry"><b>Bank Information:</b></div>
                <div className="AccountCard-entry">Credit Card #: {customerInformation.credit_card_number}</div>
                {
                    customerInformation.is_primary &&
                    <div className="AccountCard-entry"><b>This is the primary user for this account.</b></div>
                }
                <div className="AccountCard-entry">Credit Card Type: {accountInformation.credit_card_type}</div>
                <div className="AccountCard-entry">Account Balance: ${accountInformation.account_balance}</div>
                <div className="AccountCard-entry">Account Spending Limit: ${accountInformation.account_spend_limit}</div>

                <br />
                <br />
            </div>
        );
    }
}

export default AccountCard;
